// All units mm

target_radius = 2;
sensor_distance = 19.0;
led_distance = 30.0;

cylinder_iradius = 2.5;
body_thickness = 2.0;
shell_depth = 30;
shell_thickness = 2.5;
screw_column_oradius = 2.5;
screw_column_iradius = 2.8/2;       // For M3 self-tapping screw

baffle_thickness = 2;
led_baffle_1_dist = 9;
led_baffle_2_dist = 20;
sensor_baffle_1_dist = 10;
sensor_baffle_2_dist = 16;

led_beam_angle = 2*atan(target_radius/led_distance);
led_incidence_angle = 90-led_beam_angle/2;
sensor_height = sensor_distance / sqrt(2);
sensor_incidence_angle = atan(sensor_height / (sensor_height - target_radius));

assert(led_beam_angle <= 8.0);
assert(cylinder_iradius >= target_radius);
//assert(cylinder_iradius > 5.3/2);  // LED max dia
assert(led_incidence_angle - sensor_incidence_angle >= 30); // Avoid specular reflections
echo(lba = led_beam_angle);
echo(lia = led_incidence_angle);
echo(sia = sensor_incidence_angle);

$fn=50;

// mode = "design";
mode = "print_fdm";
// mode = "print_sls";


// Target
////////////////////////////////////////////////////////
//cylinder(0.1, target_radius, 0);

// Main board
////////////////////////////////////////////////////////
module refldens_main_board()
{
    color([0.15, 0.55, 0.22]) cube([80, 60, 1.6]);
}

//refldens_main_board();

// Emitter
////////////////////////////////////////////////////////
// LED: TT Electronics OVLGx0CyB9 Series
module led_OVLGx0CyB9()
{
    translate([0, 0, -5])   // A guess at the optical centre
    difference()
    {
        union()
        {
            cylinder(1.0, 5.6/2, 5.6/2);
            translate([0, 0, 1]) cylinder(8.6-5.1/2-1, 5.1/2, 5.1/2);
            translate([0, 0, 8.6-5.1/2]) sphere(5.1/2);
        };
        translate([5.1/2, -1.5, -0.01]) cube([1, 3, 1.02]);
    }
}

module refldens_emitter_board()
{
    translate([0, 0, led_distance]) rotate([0, 0, 180]) translate([-15, -12.27, 6])
    union()
    {
        difference()
        {
            color([0.15, 0.55, 0.22]) cube([25, 17, 1.6]);
            translate([6, 11, -2.5]) cylinder(5, 0.5, 0.5);
            translate([6, 11-2.54, -2.5]) cylinder(5, 0.5, 0.5);
            translate([6, 11-2.54*2, -2.5]) cylinder(5, 0.5, 0.5);
        }
        translate([6+1, 11-0.5, 1.01]) scale(1/10) text("+5V");
        translate([6+1, 11-2.54-0.5, 1.01]) scale(1/10) text("LED");
        translate([6+1, 11-2.54*2-0.5, 1.01]) scale(1/10) text("GND");
        translate([15, 12.27, -6]) rotate([180, 0, 270]) led_OVLGx0CyB9();
    }
}


// Sensor
////////////////////////////////////////////////////////
// TI OPT3002, sensor active area only
module sensor_OPT3002()
{
    translate([-0.49/2, -0.39/2, -0.06])
    cube([0.49, 0.39, 0.12]);
}

module refldens_sensor_board()
{
    translate([sensor_distance/sqrt(2), 0, sensor_distance/sqrt(2)]) rotate([0, 45, 0]) translate([-5, -7, 0.38])
    union()
    {
        difference()
        {
            color([0.15, 0.55, 0.22]) cube([20, 14, 1.6]);
            translate([15.5, 10.95, -2.5]) cylinder(5, 0.5, 0.5);
            translate([15.5, 10.95-2.54, -2.5]) cylinder(5, 0.5, 0.5);
            translate([15.5, 10.95-2.54*2, -2.5]) cylinder(5, 0.5, 0.5);
            translate([15.5, 10.95-2.54*3, -2.5]) cylinder(5, 0.5, 0.5);
        }
        translate([15.5+1, 10.95-0.5, 1.01]) scale(1/10) text("SCK");
        translate([15.5+1, 10.95-2.54-0.5, 1.01]) scale(1/10) text("SDA");
        translate([15.5+1, 10.95-2.54*2-0.5, 1.01]) scale(1/10) text("GND");
        translate([15.5+1, 10.95-2.54*3-0.5, 1.01]) scale(1/10) text("+5V");
        color([1.0, 0.0, 1.0]) translate([5, 7, -0.38]) sensor_OPT3002();
    }
}


// Optical housing
////////////////////////////////////////////////////////

// CSG strategy:
// 1. Define outer body
// 2. Subtract optical path cylinders and LED cutout
// 3. Add optical path baffles
// 4. Subtract optical path cones

module optical_housing()
{
    difference()
    {
        union()
        {
            difference()
            {
                // Body
                union()
                {
                    // LED arm
                    cylinder(led_distance+4, cylinder_iradius+body_thickness, cylinder_iradius+body_thickness);
                    // Sensor arm
                    rotate([0, 45, 0])
                    translate([0, 0, -2*target_radius/sqrt(2)])
                    cylinder(sensor_distance+2*target_radius/sqrt(2), (cylinder_iradius+body_thickness)/sqrt(2), (cylinder_iradius+body_thickness)/sqrt(2));
                }
                
                // Subtract the optical path cylinders
                // LED arm
                translate([0, 0, -1]) cylinder(led_distance+6, cylinder_iradius, cylinder_iradius);
                // Sensor arm
                rotate([0, 45, 0]) translate([0, 0, -2*target_radius/sqrt(2)])
                cylinder(sensor_distance+2*target_radius/sqrt(2)+1, cylinder_iradius/sqrt(2), cylinder_iradius/sqrt(2));

                // Subtract a cutout for the LED
                translate([0, 0, led_distance-3]) cylinder(7, 5.3/2, 5.3/2);
            }
            
            // Add optical path baffles
            // LED arm
            translate([0, 0, led_baffle_1_dist]) cylinder(baffle_thickness, cylinder_iradius+body_thickness/2, cylinder_iradius+body_thickness/2);
            translate([0, 0, led_baffle_2_dist]) cylinder(baffle_thickness, cylinder_iradius+body_thickness/2, cylinder_iradius+body_thickness/2);
            // Sensor arm
            rotate([0, 45, 0]) translate([0, 0, -2*target_radius/sqrt(2)])
            translate([0, 0, sensor_baffle_1_dist]) cylinder(baffle_thickness, (cylinder_iradius+body_thickness/2)/sqrt(2), (cylinder_iradius+body_thickness/2)/sqrt(2));
            rotate([0, 45, 0]) translate([0, 0, -2*target_radius/sqrt(2)])
            translate([0, 0, sensor_baffle_2_dist]) cylinder(baffle_thickness, (cylinder_iradius+body_thickness/2)/sqrt(2), (cylinder_iradius+body_thickness/2)/sqrt(2));
            
        }

        // Subtract the optical path cones
        // LED arm
        cylinder(led_distance, target_radius, 0.0, led_distance/2);
        // Sensor arm
        multmatrix(m = [
          [1, 0, 1, 0],
          [0, 1, 0, 0],
          [0, 0, 1, 0],
          [0, 0, 0, 1] ])
        cylinder(sensor_distance/sqrt(2), target_radius, 0.0, sensor_distance/sqrt(2)/2);
        
        // Floor
        translate([-50, -50, -100]) cube([100, 100, 100]);
    }
}

// Outer shell
////////////////////////////////////////////////////////

// Outer body profile
module outer_shape()
{
    difference()
    {
        translate([-13.5, 25, 0]) square([43.5, 20]);
        translate([-15, 38, 0]) rotate([0, 0, 45]) square([25, 15]);
        translate([-10, 16, 0]) rotate([0, 0, 45]) square([10, 15]);
    }
    translate([2.5, 0, 0]) square([27.5, 26]);
}

module outer_shell()
{
    difference()
    {
        outer_shape();
        offset(delta = -shell_thickness, chamfer = true) outer_shape();
    }
}

module screw_column()
{
    difference()
    {
        cylinder(shell_depth, screw_column_oradius, screw_column_oradius);
        translate([0, 0, -0.5]) cylinder(shell_depth+1, screw_column_iradius, screw_column_iradius);
    }
}

module enclosure()
{
    difference()
    {
        // Outer shell
        union()
        {
            translate([0, shell_depth/2, 0]) rotate([90, 0, 0]) linear_extrude(height = shell_depth, convexity = 10) outer_shell();
            translate([0, shell_depth/2, 0]) rotate([90, 0, 0]) linear_extrude(height = shell_thickness, convexity = 10) outer_shape();
            translate([0, -shell_depth/2+shell_thickness, 0]) rotate([90, 0, 0]) linear_extrude(height = shell_thickness, convexity = 10) outer_shape();

            // PCB supports
            translate([13, -shell_depth/2, led_distance+4-0.2]) cube([16, shell_depth, 2]);
            translate([13, -shell_depth/2, led_distance+4+0.2+3.6]) cube([5, shell_depth, 2]);
            translate([16, -shell_depth/2, led_distance+4+0.2]) cube([2, shell_depth, 10]);

            translate([-11, -shell_depth/2, led_distance+4-0.2]) cube([3, shell_depth, 2]);
            translate([-11, -shell_depth/2, led_distance+4+0.2+3.6]) cube([3, shell_depth, 2]);

            translate([sensor_distance/sqrt(2)-3, -shell_depth/2, sensor_distance/sqrt(2)+6]) rotate([0, 45, 0]) cube([3, shell_depth, 2]);
            translate([sensor_distance/sqrt(2)-6, -shell_depth/2, sensor_distance/sqrt(2)+3.5]) rotate([0, 45, 0]) cube([3.5, shell_depth, 2]);
            translate([sensor_distance/sqrt(2)-7, -shell_depth/2, sensor_distance/sqrt(2)+4.5]) rotate([0, 45, 0]) cube([2, shell_depth, 5.9]);

            translate([sensor_distance/sqrt(2)+7.5, -shell_depth/2, sensor_distance/sqrt(2)-10]) rotate([0, 45, 0]) cube([4.5, shell_depth, 2]);
            translate([sensor_distance/sqrt(2)+10.5, -shell_depth/2, sensor_distance/sqrt(2)-7.5]) rotate([0, 45, 0]) cube([6.5, shell_depth, 2]);
            
            // Additional patch rectangle around optical cylinder cutout
            translate([3, -shell_depth/2, 2]) cube([5, shell_depth, led_distance-4]);
            
            // Screw columns
            // Not used -- decided to go with a glue build, as contents
            // not servicable.
            /*
            translate([12.5, shell_depth/2-1, 4]) rotate([90, 0, 0]) screw_column();
            translate([26, shell_depth/2-1, 8.6]) rotate([90, 0, 0]) screw_column();
            translate([26, shell_depth/2-1, 41]) rotate([90, 0, 0]) screw_column();
            translate([-7, shell_depth/2-1, 40.3]) rotate([90, 0, 0]) screw_column();
            translate([-7.6, shell_depth/2-1, 33.3]) rotate([90, 0, 0]) screw_column();
            */
        }
        
        // Optical cylinders
        union()
        {
            // LED arm
            translate([0, 0, -0.5]) cylinder(led_distance+5, cylinder_iradius+body_thickness+0.1, cylinder_iradius+body_thickness+0.1);
            // Sensor arm
            rotate([0, 45, 0])
            translate([0, 0, -2*target_radius/sqrt(2)])
            cylinder(sensor_distance+2*target_radius/sqrt(2), (cylinder_iradius+body_thickness)/sqrt(2)+0.1, (cylinder_iradius+body_thickness)/sqrt(2)+0.1);
        }
    }
}

if (mode == "design")
{
    refldens_emitter_board();
    refldens_sensor_board();
    optical_housing();
    enclosure();
}
else if (mode == "print_fdm")
{
    rotate([0, 180, 0])
    translate([0, 50, 0])
    rotate([90, 0, 0])
    difference()
    {
        optical_housing();
        translate([-50, 0, -50]) cube([100, 100, 100]);
    }

    translate([-50, 50, 0])
    rotate([90, 0, 0])
    difference()
    {
        optical_housing();
        translate([-50, -100, -50]) cube([100, 100, 100]);
    }

    translate([0, 0, shell_depth/2])
    rotate([90, 0, 0])
    difference()
    {
        enclosure();
        translate([-50, 0, -50]) cube([100, 100, 100]);
    }

    translate([0, 0, shell_depth/2])
    rotate([0, 180, 0])
    translate([50, 0, 0])
    rotate([90, 0, 0])
    difference()
    {
        enclosure();
        translate([-50, -100, -50]) cube([100, 100, 100]);
    }
}
else if (mode == "print_sls")
{
    translate([0, 0, shell_depth/2])
    rotate([90, 0, 0])
    difference()
    {
        enclosure();
        translate([-50, 0, -50]) cube([100, 100, 100]);
    }

    translate([0, 0, shell_depth/2+6])
    rotate([90, 0, 0])
    difference()
    {
        enclosure();
        translate([-50, -100, -50]) cube([100, 100, 100]);
    }
    
    translate([0, 0, shell_depth/2+3])
    rotate([90, 0, 0])
    optical_housing();
}
else
{
    assert(false);
}